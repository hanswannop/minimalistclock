//
//  minimalistclockView.m
//  minimalistclock
//
//  Created by Hans Wannop on 11/07/11.
//  Copyright 2011 Hans Wannop. All rights reserved.
//

#import "HWMinimalistClockView.h"

@implementation HWMinimalistClockView

- (id)initWithFrame:(NSRect)frame isPreview:(BOOL)isPreview
{
    self = [super initWithFrame:frame isPreview:isPreview];
    if (self) {
        NSOpenGLPixelFormatAttribute attributes[] = {
            NSOpenGLPFAAccelerated,
            NSOpenGLPFADepthSize, 16,
            NSOpenGLPFAMinimumPolicy,
            NSOpenGLPFAClosestPolicy,
            0 };
        NSOpenGLPixelFormat *format;
        
        format = [[[NSOpenGLPixelFormat alloc] initWithAttributes:attributes] autorelease];
        glView = [[HWMinimalistClockOpenGLView alloc] initWithFrame:NSZeroRect pixelFormat:format];
        
        if(!glView)
        {
            NSLog(@"Couldn't initialize OpenGL view.");
            [self autorelease];
            return nil;
        }
        
        [self addSubview:glView];
        [self setUpOpenGL];
        
        [self setAnimationTimeInterval:1/30.0];
        
        ScreenSaverDefaults * screenSaverDefaults = [self defaults];
        
        NSDictionary* defaultDict = [NSDictionary dictionaryWithObject:kDefaultsYesString forKey:kDefaultsChunkyHoursKey];
        
        [screenSaverDefaults registerDefaults: defaultDict];
        
        chunkyHours = [screenSaverDefaults boolForKey:kDefaultsChunkyHoursKey];
        chunkyMinutes = YES;
        chunkySeconds = NO;
        
    }
    return self;
}

- (void)startAnimation
{
    [super startAnimation];
}

- (void)stopAnimation
{
    [super stopAnimation];
}

- (void)drawRect:(NSRect)rect
{
    [super drawRect:rect];
    
    [[glView openGLContext] makeCurrentContext];
    
    glClear(GL_COLOR_BUFFER_BIT| GL_DEPTH_BUFFER_BIT);
    glLoadIdentity();
    
    GLfloat smoothingExtra;
    glBegin(GL_QUADS);
    //Draw Hours
    glColor4f(1.0f, 0.0f, 0.0f, 1.0f);
    glVertex2f(0.0f,0.0f);
    glVertex2f(0.0f,rect.size.height);
    glVertex2f((rect.size.width/12.0f)*(GLfloat)hours + smoothingExtra,rect.size.height);
    glVertex2f((rect.size.width/12.0f)*(GLfloat)hours + smoothingExtra,0.0f);
    
    //Draw Minutes
    if (chunkyMinutes) {
        smoothingExtra = 0.0f;
    } else {
        smoothingExtra = 0.0f;    }
    
    glColor4f(0.0f, 0.0f, 1.0f, 0.5f);
    glVertex2f(0.0f,0.0f);
    glVertex2f(0.0f,rect.size.height);
    glVertex2f((rect.size.width/60.0f)*(GLfloat)minutes + smoothingExtra,rect.size.height);
    glVertex2f((rect.size.width/60.0f)*(GLfloat)minutes + smoothingExtra,0.0f);
    
    //Draw Seconds
    if (chunkySeconds) {
        smoothingExtra = 0.0f;
    } else {
        smoothingExtra = ((rect.size.width/60.0f/1000.0f) * milliseconds);
    }
    
    glColor4f(0.0f, 0.0f, 1.0f, 0.2f);
    glVertex2f(0.0f,0.0f);
    glVertex2f(0.0f,rect.size.height);
    glVertex2f((rect.size.width/60.0f) * seconds + smoothingExtra, rect.size.height);
    glVertex2f((rect.size.width/60.0f) * seconds + smoothingExtra, 0.0f);
    glEnd();
    
    //Draw 5min/hour markers
    
    glColor4f(0.0f, 0.0f, 0.0f, 1.0f);
    for (int i = 1; i<12; i++) {
        if (i % 3==0){
            glLineWidth(3.0f);
            glBegin(GL_LINES);
            glVertex2f((rect.size.width/12.0f)*i-0.5f,rect.size.height/15.0);
            glVertex2f((rect.size.width/12.0f)*i-0.5f,0.0f);
            glEnd();
        } else{
            glLineWidth(1.0f);
            glBegin(GL_LINES);
            glVertex2f((rect.size.width/12.0f)*i-0.5f,rect.size.height/20.0);
            glVertex2f((rect.size.width/12.0f)*i-0.5f,0.0f);
            glEnd();
        }
    }
    
    glFlush();
}

- (void)animateOneFrame
{
    // Update the clock
    NSDate *date = [NSDate date];
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *dateComponents = [calendar components:(NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit) fromDate:date];
    hours = [dateComponents hour]%12;
    minutes = [dateComponents minute];
    seconds = [dateComponents second];
    double interval = [date timeIntervalSince1970];
    double fractionalPart = 0.0;
    double integralPart = 0.0;
    fractionalPart = modf(interval, &integralPart);
    milliseconds = (NSInteger)(fractionalPart*1000.0);
    [calendar release];
    
    // Redraw
    
    [self setNeedsDisplay:YES];
    return;
}

- (BOOL)hasConfigureSheet
{
    return YES;
}

- (NSWindow*)configureSheet
{
    if (!configureSheet) {
        if (![NSBundle loadNibNamed:kConfigureSheetNIB owner:self])
        {
            NSLog(@"Oops, something went wrong. I couldnt show the options sheet!");
            NSBeep();
        }
        [chunkyHoursCheckbox setState:chunkyHours];
        [chunkyMinutesCheckbox setState:chunkyMinutes];
        [chunkySecondsCheckbox setState:chunkySeconds];
    }
    return configureSheet;
}

- (void)setUpOpenGL
{
    [[glView openGLContext] makeCurrentContext];
    
    glShadeModel(GL_SMOOTH);
    glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
    glClearDepth(1.0f);
    glEnable(GL_DEPTH_TEST);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_BLEND);
    glDepthFunc(GL_LEQUAL);
}

- (void)setFrameSize:(NSSize)newSize
{
    [super setFrameSize: newSize];
    [glView setFrameSize: newSize];
    
    [[glView openGLContext] makeCurrentContext];
    
    // Reshape
    glViewport(0, 0, (GLsizei)newSize.width, (GLsizei)newSize.height);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluOrtho2D(0, (GLfloat)newSize.width, 0, (GLfloat)newSize.height);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    
    [[glView openGLContext] update];
}

- (void)dealloc
{
    [glView removeFromSuperview];
    [glView release];
    [super dealloc];
}

- (IBAction)cancelSheetAction:(id)sender
{
    [[NSApplication sharedApplication] endSheet:configureSheet];
}

- (IBAction)okSheetAction:(id)sender
{
    chunkyHours = [chunkyHoursCheckbox state];
    chunkyMinutes = [chunkyMinutesCheckbox state];
    chunkySeconds = [chunkySecondsCheckbox state];
    
    [[NSApplication sharedApplication] endSheet:configureSheet];
}

- (ScreenSaverDefaults*) defaults {
    return [ScreenSaverDefaults defaultsForModuleWithName:kDefaultsModuleName];
}

@end
