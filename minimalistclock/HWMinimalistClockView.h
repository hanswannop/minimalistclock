//
//  minimalistclockView.h
//  minimalistclock
//
//  Created by Hans Wannop on 11/07/11.
//  Copyright 2011 Hans Wannop. All rights reserved.
//

#import <ScreenSaver/ScreenSaver.h>
#import <OpenGL/gl.h>
#import <OpenGL/glu.h>
#import "HWMinimalistClockOpenGLView.h"

#define kConfigureSheetNIB @"ConfigureSheet"
#define kDefaultsModuleName @"MinimalistClockDefaults"
#define kDefaultsChunkyHoursKey @"chunkyHoursDefault"
#define kDefaultsChunkyMinutesKey @"chunkyMinutesDefault"
#define kDefaultsChunkySecondsKey @"chunkySecondsDefault"
#define kDefaultsYesString @"YES"
#define kDefaultsNoString @"NO"


@interface HWMinimalistClockView : ScreenSaverView
{
    HWMinimalistClockOpenGLView *glView;
    NSInteger hours;
    NSInteger minutes;
    NSInteger seconds;
    NSInteger milliseconds;
    BOOL chunkyHours;
    BOOL chunkyMinutes;
    BOOL chunkySeconds;
    
    IBOutlet NSWindow * configureSheet;
    IBOutlet id chunkyHoursCheckbox;
    IBOutlet id chunkyMinutesCheckbox;
    IBOutlet id chunkySecondsCheckbox;
    
}

- (IBAction)cancelSheetAction:(id)sender;
- (IBAction)okSheetAction:(id)sender;

- (void)setUpOpenGL;
- (ScreenSaverDefaults *) defaults;

@end
