//
//  HWMinimalistClockOpenGLView.m
//  MinimalistClock
//
//  Created by Hans Wannop on 11/07/11.
//  Copyright 2011 Hans Wannop. All rights reserved.
//

#import "HWMinimalistClockOpenGLView.h"

@implementation HWMinimalistClockOpenGLView

- (id)init
{
    self = [super init];
    if (self) {
        // Initialization code here.
    }
    
    return self;
}


- (BOOL)isOpaque
{
    return NO;
}
@end
